# -*- coding: utf-8 -*-
{
    'name': "Tuple for Selection",

    'summary': """
        Defines a tuple-like data.
    """,

    'description': """
        Module defines a tuple-like data to support the domains in fields 
        of type 'Selection'; so it is intended to replace 'Selection' fields 
        whenever domains need to be used on them.
    """,

    'author': "Yulien Paz Morales",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Utils',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        #'security/user_groups.xml',
        #'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}