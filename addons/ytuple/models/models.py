# -*- coding: utf-8 -*-

from odoo import models, fields

class ytuple(models.Model):
    _name = 'ytuple.ytuple'
    _rec_name = 'value'

    category = fields.Char(size=50)
    description = fields.Text()
    name = fields.Char(string="Key", size=25, required=True)
    parent_id = fields.Many2one('ytuple.ytuple')
    value = fields.Char(size=25, required=True)
