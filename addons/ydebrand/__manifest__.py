# -*- coding: utf-8 -*-
{
    'name': "Tracfone Odoo Debrand",

    'summary': """
        Remove Odoo images/texts and add myself's.
        """,

    'description': """
        This module removes some Odoo images and texts, like 'Powered by
        Odoo', and replaces them by specific ones.
    """,

    'author': "Yulien Paz Morales",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Design',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/resources.xml',
        'views/templates.xml',
    ],
    'qweb': [
        'static/src/xml/inherit_base.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    'css': ['static/src/css/ydebrand.css'],
}