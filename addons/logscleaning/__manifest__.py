# -*- coding: utf-8 -*-
{
    'name': "Files Cleaning",

    'summary': """
        Cleaner/Remover of files in servers/paths according to filters.""",

    'description': """
        This module is intended to connect through ssh to other servers using 
        public key. Files to be checked can be filtered by name/extension, 
        and they can be cleaned or removed by time and/or size.

        To filter files by name, use Unix shell-style wildcards, which are not 
        the same as regular expressions. The special characters used in 
        shell-style wildcards are:

        * matches everything

        ? matches any single character

        [seq] matches any character in seq

        [!seq] matches any character not in seq


        It keeps a trace log of everything cleaned/removed. Such logs are 
        saved on those servers defined as *jumpbox*.

        Important!
        
        - When added more than one rule to the same path, it means the 
        filtered files are those that match *all* rules together:
        (rule1 AND rule2 AND ... AND rule n)
        
        - If same path is added (with different rules) to the same server 
        or cluster, it means every rule will be applied separately.

        Let's say we need to remove files which name is tas_1.log0001, 
        tas_1.log0002, ..., tas_1.log0099..., etc., only if they were updated 
        more than 24 hours ago and have more than 50 MB. Also, we need to 
        remove those files updated more than 2 weeks ago. Location of these 
        files is /home/weblogic/config/tas.training.com/logs/
        To put on practice this module we would use this approach:
        
        - Rule Model:
            Create 4 rules:
                1 - Name: tas_1.log*

                2 - Size: >50 MB

                3 - Time: >24 Hours

                4 - Time: >14 Days

        - Path Model:
            Create 2 paths with rules as follows:
                1 - Path: /home/weblogic/config/tas.training.com/logs/
                    Rules: Name: tas_1.log* AND Size: >50 MB AND Time: >24 Hours
                2 - Path: /home/weblogic/config/tas.training.com/logs/
                    Rules: Time: >14 Days
        - Server Model:
            Create 1 server, and add above 2 paths/rules.
    """,

    'author': "Yulien Paz Morales",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Automation',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['ytuple', 'ypopup', 'yutils'],

    # always loaded
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'data/criterias.xml',
        'data/measurements.xml',
        'data/server_types.xml',
        'data/actions.xml',
        'views/resources.xml',
        'views/server_type.xml',
        'views/rule.xml',
        'views/path.xml',
        'views/server.xml',
        'views/server_group.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}