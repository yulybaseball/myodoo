# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Path(models.Model):
    _name = 'logscleaning.path'
    _inherit = 'logscleaning.updater'
    _rec_name = 'path'

    # Private functions
    @api.multi
    def _check_rules_actions(self):
        self.ensure_one()
        action_ids = self.rule_ids.mapped('action_id.id')
        msg = _("Rules for same path shouldn't have different actions.")
        return msg if not len(set(action_ids)) == 1 else False

    path = fields.Char(required=True)
    rule_ids = fields.Many2many('logscleaning.rule', string="Rules", 
                                ondelete="set null")
    rules_resume = fields.Text(compute='_compute_rules_resume')

    # Constraints
    @api.one
    @api.constrains('path', 'rule_ids')
    def _check_path_rule_ids_constraint(self):
        for path_rule in self.search([('path', '=', self.path), 
                                      ('id', '!=', self.id)]):
            path_rule.rule_ids.ids.sort()
            self.rule_ids.ids.sort()
            if path_rule.rule_ids.ids == self.rule_ids.ids:
                raise ValidationError(
                    _('There is already a path with the same rule(s),' + 
                      ' or no rules at all!'))

    @api.one
    @api.constrains('rule_ids', 'path')
    def _check_rules_actions_constraint(self):
        if self.rule_ids:
            check_msg = self._check_rules_actions()
            if check_msg:
                raise ValidationError(check_msg)

    # Computed fields' functions
    @api.depends('rule_ids', 'rule_ids.name')
    def _compute_rules_resume(self):
        for rec in self:
            rec.rules_resume = _(" AND ").join(rec.rule_ids.mapped('name')) \
                               if rec.rule_ids else _('[no rules]')

    # On-changes
    @api.onchange('path')
    def onchange_path(self):
        if self.path:
            lc = self.path[-1]
            self.path = "{}{}".format(self.path.strip(), 
                                      '/' if lc != '/' else '')

    @api.onchange('rule_ids')
    def onchange_rule_ids(self):
        if self.rule_ids:
            check_msg = self._check_rules_actions()
            if check_msg:
                return {'warning': 
                            {'title': _("Warning"), 'message': check_msg}}
