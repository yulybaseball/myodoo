# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Rule(models.Model):
    _name = 'logscleaning.rule'
    _inherit = 'logscleaning.updater'

    # Default values' functions
    @api.model
    def _default_action_id(self):
        domain = self._criteria_id_domain()
        return self.env['ytuple.ytuple'].search(domain, limit=1)

    # Private functions
    @api.model
    def _criteria_id_domain(self):
        return [('category', '=', 'file_actions')]

    @api.one
    def _check_files_filter(self):
        func = getattr(type(self), 
                       "_check_files_filter_{}".format(self.criteria_id.name))
        func(self)

    def _check_files_filter_name(self):
        # for rule in self.search([('criteria_id', '=', self.criteria_id.id), 
        #                          ('id', '!=', self.id)]):
        #     if rule.files_filter == self.files_filter:
        #         raise ValidationError(_('Rule already exists!'))
        pass

    def _check_files_filter_size(self):
        self._check_files_filter_common()

    def _check_files_filter_time(self):
        self._check_files_filter_common()

    def _check_files_filter_common(self):
        if not type(self)._allowed_common_filter(self.files_filter):
            raise ValidationError(_('Filter is not correct.'))

    action_id = fields.Many2one('ytuple.ytuple', ondelete='restrict', 
                                required=True, default=_default_action_id, 
                                domain=_criteria_id_domain)
    criteria_id = fields.Many2one('ytuple.ytuple', required=True, 
                        domain=[('category', '=', 'files_select_criteria')],
                        ondelete='restrict')
    criteria_id_name = fields.Char(compute='_compute_criteria_id_name')
    files_filter = fields.Char(string="Filter", required=True, size=50)
    measurement_id = fields.Many2one('ytuple.ytuple', ondelete='restrict')
    name = fields.Char(compute='_compute_name', store=True)

    # Constraints
    _sql_constraints = [
        ('rule_unique_with_measurement', 
        'unique(criteria_id, files_filter, measurement_id, action_id)', 
        'Rule already exists (using Measurement)!')
    ]

    @api.one
    @api.constrains('files_filter')
    def _check_files_filter_constraint(self):
        self._check_files_filter()

    # Computed fields' functions
    @api.one
    @api.depends('criteria_id')
    def _compute_criteria_id_name(self):
        if self.criteria_id:
            self.criteria_id_name = self.criteria_id.name
        else:
            self.criteria_id_name = None

    @api.depends('criteria_id', 'files_filter', 'measurement_id', 'action_id')
    def _compute_name(self):
        for rec in self:
            if rec.criteria_id and rec.files_filter:
                rec.name = "{}: {} {} [{}]".format(
                    rec.criteria_id.value, rec.files_filter, 
                    rec.measurement_id.value if rec.measurement_id else "",
                    rec.action_id.value)

    # Static methods
    @staticmethod
    def _allowed_common_filter(f_filter):
        symbols = ['>', '<', '<=', '>=']
        oper, value = Rule._split_filter(f_filter)
        return oper in symbols and value is not None

    @staticmethod
    def _split_filter(f_filter):
        f_no_spaces = "".join(f_filter.split())
        i = 0
        try:
            for symbol in f_no_spaces:
                if symbol.isdigit():
                    return f_no_spaces[:i], int(f_no_spaces[i:])
                i += 1
        except Exception:
            pass
        return f_no_spaces, None

    # On-changes
    @api.onchange('criteria_id')
    def onchange_criteria(self):
        self.measurement_id = False
        self.files_filter = False

    @api.onchange('files_filter')
    def onchange_files_filter(self):
        if self.files_filter:
            f, v = type(self)._split_filter(self.files_filter)
            self.files_filter = "{}{}".format(f, v if v is not None else "")
