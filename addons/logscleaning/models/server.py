# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Server(models.Model):
    _name = 'logscleaning.server'
    _inherit = 'logscleaning.updater'

    # Private functions
    @api.multi
    def _summary(self):
        self.ensure_one()
        summary = {}
        rules_count = 1
        for path in self.path_ids.sorted('path'):
            if path.path not in summary.keys():
                rules_count = 1
                summary[path.path] = "{})\t{}".format(rules_count, 
                                                      path.rules_resume)
            else:
                summary[path.path] += ("\n\t{})\t{}".format(rules_count, 
                                                            path.rules_resume))
            rules_count += 1
        return summary

    jumpbox = fields.Boolean()
    jump_server_id = fields.Many2one('logscleaning.server')
    name = fields.Char(required=True, size=40)
    path_ids = fields.Many2many('logscleaning.path', string="Paths & Rules")
    rules_summary = fields.Text(compute='_compute_rules_summary')
    server_type_id = fields.Many2one('logscleaning.server.type', 
                                     required=True, string="Server Type")
    script_path = fields.Char()
    username = fields.Char(required=True)

    # Constraints
    @api.one
    @api.constrains('name', 'path_ids')
    def _check_name_path_ids_constraint(self):
        for server in self.search([('name', '=', self.name), 
                                   ('id', '!=', self.id)]):
            server.path_ids.ids.sort()
            self.path_ids.ids.sort()
            if server.path_ids.ids == self.path_ids.ids:
                raise ValidationError(
                    _('There is already a server with the same name and ' + 
                      'path/rule(s) combination, or no rules at all!'))

    # Computed fields' functions
    @api.multi
    @api.depends('path_ids', 'server_type_id', 'path_ids.path', 
                 'path_ids.rule_ids')
    def _compute_rules_summary(self):
        self.ensure_one()
        path_strs = self.path_ids.mapped('path')
        path_strs = list(set(path_strs))
        path_strs.sort()
        summary_str = ""
        if path_strs:
            summary = self._summary()
            for path_str in path_strs:
                sep = '======================================================='
                summary_str += ("\n" if summary_str else "")
                summary_str += ("{}: {}\n{}:\n\t{}\n{}".format(
                    _("Path"), path_str, _("Rules"), summary[path_str], sep))
        self.rules_summary = summary_str

    # On-changes
    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            self.name = "".join(self.name.split())

    @api.onchange('script_path')
    def onchange_script_path(self):
        if self.script_path:
            lc = self.script_path[-1]
            self.script_path = "{}{}".format(self.script_path.strip(), 
                                             '/' if lc != '/' else '')

    # Buttons' actions
    @api.multi
    def test_conn(self):
        self.ensure_one()
        uname = self.server_type_id.username
        pkey = self.server_type_id.pass_key
        rhost = self.name
        __, error = self.env['yutils'].connect_remotely(uname, pkey, rhost)
        if error:
            caption = "ERROR"
            message = error
            desc = ("Please note that any change you have made won't be " + 
                    "propagated to jumpbox {}.")
            messtype = 'warning'
        else:
            caption = "SUCCESS"
            message = ("Connection to {} was successful!").format(rhost)
            desc = ("Any change you have made will be " + 
                    "propagated to jumpbox {}.")
            messtype = 'success'
        return self.env['ypopup'].show_popup(caption, message, 
                                             desc.format(rhost), messtype)

    @api.multi
    def updateJSON_confirm(self):
        self.ensure_one()
        caption = "Confirmation"
        message = "Are you sure you want to update configuration file?"
        messtype = 'confirmation'
        action = "self.env['{}']._updateJSON({}, True)".format(self._name, 
                                                               self.id)
        return self.env['ypopup'].show_popup(caption, message, 
                                             message_type=messtype,
                                             action=action)
