# -*- coding: utf-8 -*-

from odoo import models, api, _

CONF_FILE_NAME = "conf.json"

class LogsCleaningUpdater(models.AbstractModel):
    _name = 'logscleaning.updater'

    # Private methods
    @api.multi
    def _gather_data(self, jump_server):
        data = {}
        servers = self.env['logscleaning.server'].search([
                            ('jump_server_id.id', '=', jump_server.id)])
        if not jump_server.jump_server_id:
            servers = jump_server + servers
        for server in servers:
            if server.name not in data:
                data[server.name] = { 'user': server.username,
                                      'paths': {} }
            paths = server.path_ids
            if paths:
                for path in paths:
                    rule_list = []
                    if path.path not in data[server.name]['paths']:
                        data[server.name]['paths'][path.path] = []
                    for rule in path.rule_ids:
                        r = {'criteria': rule.criteria_id.name,
                            'filter': rule.files_filter,
                            'measurement': rule.measurement_id.name,
                            'action':  rule.action_id.name}
                        rule_list.append(r)
                    data[server.name]['paths'][path.path].append(rule_list)
        return data

    @api.multi
    def _get_jumpboxserver_credentials(self, server):
        username = server.server_type_id.username
        pass_key = server.server_type_id.pass_key
        return (username, pass_key)

    @api.multi
    def _updateJSON_server(self, jump_server, data=None):
        if jump_server.jumpbox:
            username, pass_key = self._get_jumpboxserver_credentials(jump_server)
            if not data:
                data = self._gather_data(jump_server)
            js_name = jump_server.name
            err = self.env['yutils'].update_file_content(username, pass_key, 
                                    js_name, data, jump_server.script_path, 
                                    CONF_FILE_NAME, keep_backup=True)
            if err:
                return "{}: {}\n{}: {}".format(_("Server"), 
                                            js_name, _("Error"), err)
            return ""
        return _("Configuration file doesn't exist in non-jumpbox servers.")

    @api.multi
    def _updateJSON(self, jumpserver_id=None, feedback=False):
        message = ""
        if jumpserver_id:
            jb_server = self.env['logscleaning.server'].browse(jumpserver_id)
            if jb_server:
                message = self._updateJSON_server(jb_server)
        else:
            for jump_server in self.env['logscleaning.server'].search([
                                                    ('jumpbox', '=', True)]):
                data = self._gather_data(jump_server)
                message += self._updateJSON_server(jump_server, data=data)
        print(message)
        if feedback and message:
            caption = _("WARNING")
            messtype = 'warning'
            gentext = _("Following jumpbox server(s) couldn't be updated:")
            return self.env['ypopup'].show_popup(caption, gentext, message,
                                                 message_type=messtype)

    # Rewritten methods
    @api.multi
    def write(self, vals):
        result = super(LogsCleaningUpdater, self).write(vals)
        self._updateJSON()
        return result

    @api.model
    def create(self, vals):
        result = super(LogsCleaningUpdater, self).create(vals)
        self._updateJSON()
        return result

    @api.multi
    def unlink(self):
        result = super(LogsCleaningUpdater, self).unlink()
        self._updateJSON()
        return result
