# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ServerGroup(models.Model):
    _name = 'logscleaning.server.group'
    _inherit = 'logscleaning.updater'

    name = fields.Char(required=True, size=50)
    path_ids = fields.Many2many('logscleaning.path', string="Paths & Rules")
    server_ids = fields.Many2many('logscleaning.server', string="Servers",
                                  required=True)

    # On-changes
    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            self.name = "".join(self.name.split())
