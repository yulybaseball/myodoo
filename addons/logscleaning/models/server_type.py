# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ServerType(models.Model):
    _name = 'logscleaning.server.type'
    _inherit = 'logscleaning.updater'

    pass_key = fields.Text(string="Key", required=True)
    server_type_id = fields.Many2one('ytuple.ytuple', ondelete='restrict', 
                        domain=[('category', '=', 'server_type')],
                        required=True)
    username = fields.Char(size=25, required=True)

    # Constraints
    _sql_constraints = [
        ('server_type_id_unique',  'unique(server_type_id)',  
        'Server type already exists!'),
    ]

    @api.multi
    @api.depends('server_type_id')
    def name_get(self):
        return [(st.id, str(st.server_type_id.value)) for st in self]
