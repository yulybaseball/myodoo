# -*- coding: utf-8 -*-

import paramiko
import io
import subprocess
import pipes
import shlex

from odoo import models, fields, api

# class openacademy(models.Model):
#     _name = 'openacademy.openacademy'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100


class AutomatedJob(models.Model):
    _name = 'automated_jobs.automated_job'

    name = fields.Char(required=True)
    description = fields.Text()
    server = fields.Char(required=True)
    path = fields.Char(required=True)
    script = fields.Char(required=True)
    username = fields.Char(required=True)
    password = fields.Char(password=True)
    pass_key = fields.Text()

    def run_job(self):

        my_key = self.pass_key
        pkey = paramiko.RSAKey.from_private_key(io.StringIO(my_key))
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname=self.server, username=self.username, pkey=pkey)

        # client = pm.SSHClient()
        # client.load_system_host_keys()
        # client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
        # client.set_missing_host_key_policy(AllowAllKeys())
        
        command = "cd {};./{}".format(self.path, self.script)
        # command = "cat applications.ALL"
        print("Trying to execute this command: {}".format(command))
        # result = ssh.exec_command(command)
        stdin, stdout, ssh_stderr = ssh.exec_command(command)
        out = stdout.readlines()
        ssh.close()
        print("OUT: {}".format(out))
        # stdin.flush()
        # result = ssh.exec_command("pwd")
        # print("RESULT: {}".format(result))
        return True

    # def _cmd(self, command):
    #     """Execute command and return the output."""
    #     p = subprocess.Popen(shlex.split(command), stdin=subprocess.PIPE,
    #                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #     (stdout, stderr) = p.communicate()
    #     return stdout