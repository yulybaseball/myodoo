# -*- coding: utf-8 -*-

from odoo import models

import paramiko
import io
import json
import os
import sys
import time

from datetime import datetime
from pytz import timezone

sys.path.append("/home/proapsup/Users/Yulien/")
from pssapi.logger import logger
from pssapi.utils import conn

LOG_FILE = "/home/proapsup/Users/Yulien/Odoo/myodoo/logs/logscleaner.log"
TIME_ZONE = timezone('America/New_York')
def timetz(*args):
    return datetime.now(TIME_ZONE).timetuple()

logger_fmtter = {
    'INFO': '%(asctime)s - %(process)d - %(message)s',
    'DEFAULT': '%(asctime)s - %(process)d - %(levelname)s - %(message)s'
}
_logger = logger.load_logger(log_file_name=LOG_FILE, cgi=True, **logger_fmtter)
_logger.handlers[0].formatter.converter = timetz

class YUtils(models.AbstractModel):
    _name = 'yutils'

    def connect_remotely(self, uname, pass_key, remote_host, keep_open=False):
        ssh = paramiko.SSHClient()
        try:
            pkey = paramiko.RSAKey.from_private_key(io.StringIO(pass_key))
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=remote_host, username=uname, pkey=pkey)
            return ssh, False
        except Exception as e:
            print("AN EXCEPTION OCCURED WHEN TRYING TO CONNECT TO {}: {}".\
                  format(remote_host, str(e)))
            return False, str(e)
        finally:
            if not keep_open:
                ssh.close()

    def _sftp_file_exists(self, ftp, file_name):
        try:
            ftp.stat(file_name)
            return True
        except Exception:
            return False

    def _create_backup(self, ftp, file_name):
        b_file_name = "{}.bak".format(file_name)
        if self._sftp_file_exists(ftp, b_file_name):
            # delete old backup
            ftp.remove(b_file_name)
        if self._sftp_file_exists(ftp, file_name):
            # create backup
            ftp.rename(file_name, b_file_name)

    def _create_local_backup(self, file_name):
        b_file_name = "{}.bak".format(file_name)
        if os.path.isfile(b_file_name):
            # delete old backup
            os.remove(b_file_name)
        if os.path.isfile(file_name):
            # create backup
            os.rename(file_name, b_file_name)

    def _update_local_file_content(self, file_name, path, data, keep_backup=False):
        cpath = os.path.join(path, file_name)
        if keep_backup:
            self._create_local_backup(cpath)
        with open(cpath, 'w') as _conf_file:
            _conf_file.write(json.dumps(data))

    def update_file_content(self, uname, pass_key, remote_host, data, path, 
                            file_name, keep_backup=False):
        result = None
        current_server = conn.current_server()
        if current_server == remote_host:
            self._update_local_file_content(file_name, path, data, keep_backup)
        else:
            try:
                ssh, error = self.connect_remotely(uname, pass_key, remote_host, 
                                                keep_open=True)
                if not ssh:
                    raise paramiko.SSHException(error)
                ftp = ssh.open_sftp()
                # ftp.chdir(path)
                # above line was commented out because it was failing in AIX 
                # servers with error: mode out of range
                # to change ditectory we are setting the path directly to 
                # the private variable from Paramiko.sftp_client module
                # TODO: change this back to using chdir()
                ftp._cwd = path.encode('utf-8')
                if keep_backup:
                    self._create_backup(ftp, file_name)
                remote_file = ftp.open(file_name, 'w')
                remote_file.write(json.dumps(data))
                remote_file.flush()
                ftp.close()
            except Exception as e:
                _logger.error(str(e))
                print("AN EXCEPTION OCCURED WHEN TRYING TO UPDATE FILE CONTENT" + 
                    " REMOTELY IN {}: {}".format(remote_host, str(e)))
                result = str(e)
            finally:
                if ssh:
                    ssh.close()
        return result
