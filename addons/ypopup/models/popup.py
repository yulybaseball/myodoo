# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Popup(models.TransientModel):
    _name = 'ypopup'

    # Private methods
    @api.model
    def _get_view_by_message_type(self, message_type):
        return self.env.ref('ypopup.ypopup_form_confirmation_view') \
                if message_type.lower() == 'confirmation' \
                else self.env.ref('ypopup.ypopup_form_message_view')

    description = fields.Text()
    image = fields.Html(compute="_compute_image")
    message = fields.Text(required=True)
    message_type = fields.Char()

    # Computed fields' functions
    @api.multi
    @api.depends('message_type')
    def _compute_image(self):
        self.ensure_one()
        if self.message_type:
            imgtag = ('<img src="{}" class="img img-responsive"' + 
                      'style="height: 40; width: 40px;" />')
            img_url = '{}/static/src/img/{}.png'.format(self._name, 
                                                        self.message_type)
            self.image = imgtag.format(img_url)
        else:
            self.image = ''

    # Buttons' actions
    @api.model
    def show_popup(self, caption, message, description=None, 
                   message_type=None, action=None):
        instance = self.create({'message': _(message),
                                       'description': _(description),
                                       'message_type': message_type})
        view = self._get_view_by_message_type(message_type)
        return {
            'type': 'ir.actions.act_window',
            'name': _(caption),
            'res_model': self._name,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'res_id': instance.id,
            'view_id': view.id,
            'context': {'confirmed_action': action},
        }

    @api.multi
    def confirmed(self):
        self.ensure_one()
        act = 'confirmed_action'
        if act in self.env.context and self.env.context[act]:
            return eval(self.env.context[act])
